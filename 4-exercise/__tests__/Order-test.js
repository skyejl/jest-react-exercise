import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import axios from 'axios';
import 'babel-polyfill';

import OrderComponent from '../Order';

jest.mock('axios'); // Mock axios模块

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试
  // setup组件
  // const usernameInput = getByTestId(container, 'username-input')
  const { getByLabelText, getByTestId } = render(<OrderComponent />);
  const button = getByLabelText('submit-button');

  // Mock数据请求
  const resp = { data: { status: '已完成' } };
  axios.get.mockResolvedValue(resp);

  // or you could use the following depending on your use case:
  // axios.get.mockImplementation(() => Promise.resolve(resp))
  // 触发事件
  await fireEvent.click(button);

  // 给出断言
  const p = getByTestId('status');
  expect(p).toHaveTextContent('已完成');
  // --end->
});
